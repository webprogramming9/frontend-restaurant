export default interface Product {
  id?: number;
  username: string;
  name: string;
  password: string;
}
