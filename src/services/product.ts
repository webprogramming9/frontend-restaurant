import type Product from "@/types/Product";
import http from "./axios";

function getProducts() {
  return http.get("/product");
}

function saveProduct(product: Product) {
  return http.post("/product", product);
}

function updateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

export default { getProducts, saveProduct, updateProduct, deleteProduct };
